package hello;



import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

// Comment Assignment 9 - Whitney Pulliam
// Starting class (3-4)

// comment written 2/24/2022: made it say my name and world in French
// comment written 2/24/2022: made it say my first and last name, and a assertFale test
//comment by denny
public class TestGreeter {

   private Greeter g = new Greeter();

   @Test
   @DisplayName("Test for Empty Name")
   public void testGreeterEmpty() 

   {
      assertEquals(g.getName(),"");
      assertEquals(g.sayHello(),"Hello!");
   }

   @Test
   @DisplayName("Test for Angelo")
   public void testGreeterTed() 

   {
      g.setName("Angelo");
      assertEquals(g.getName(),"Angelo");
      assertEquals(g.sayHello(),"Hello Angelo!");
   }

   @Test
   @DisplayName("Test for Name= 'Monde'")
   public void testGreeter() 
   {

      g.setName("Monde");
      assertEquals(g.getName(),"Monde");
      assertEquals(g.sayHello(),"Hello Monde!");
   }

   @Test
   @DisplayName("Test for name = 'Angel Carballo'")
   public void testGreeterCustom1()
   {
      g.setName("Angel Carballo");
      assertEquals(g.getName(), "Angel Carballo");
      assertEquals(g.sayHello(), "Hello Angel Carballo!");
   }
    @Test
    @DisplayName("Test for Whitney")
    public void testGreeterWhitney()
    {
        g.setName("Whitney");
        assertFalse((g.getName() == "Denny"),"This is not Denny's Greeter, right?");
    }

   @Test
   @DisplayName("Test for name = 'Denny'")
   public void testGreeterDenny()
   {
      g.setName("Denny");
      assertFalse(g.getName() == "Ted");
      assertEquals(g.sayHello(), "Hello Denny!");
   }
   @Test
   @DisplayName("Test for name = '? Is this thing on?'")
   public void testGreeterCustom2()
   {
      // using AssertFalse
      boolean bin = false;
      g.setName("? Is this thing on?");

      if (g.getName() != "? This thing is on!")
      {
         assertFalse(bin, "Failure on 'getName()'");
         assertFalse(bin, "Failure on 'sayHello()'");
      }

      // if thing is in fact, On
      else
      {
         bin = true;
         assertTrue(bin, "I finally got the build to be successful!" +
                 " (turns out asserFalse != assertFalse)");
      }
   }

   @Test
   @DisplayName("Test for Buon Giorno! My Italian Greeting")
   public void testGreeterAngelBuonGiorno()
   {
       g.setName("Buon Giorno! My Italian Greeting");

       assertEquals(g.getName(), "Buon Giorno! My Italian Greeting");
       assertEquals(g.sayHello(), "Hello Buon Giorno! My Italian Greeting!");
   }

   


   @Test
   @DisplayName("A14 Test for Whitney")
    public void testA14Whitney()
    {
        g.setName("wpp12");
        assertFalse((g.getName() == "Whitney"),"This is not Whitney's first Greeter, right?");
    }

    @Test
    @DisplayName("A14 Test for Denny")
    public void testA14Denny()
    {
        g.setName("D_D365");
        assertFalse((g.getName() == "Denny"),"This is not Denny's Greeter, right?");
    }

   @Test
    @DisplayName("A14 Test for Carlie")
    public void testA14Carlie()
    {
        g.setName("CAK177");
        assertFalse((g.getName() == "Carlie"),"This is not Carlie's Greeter, right?");
    }
   @Test
   @DisplayName("A14 Test for Pedro")
   public void testA14Pedro()
    {
        g.setName("PJA36");
        assertFalse((g.getName() == "Pedro"),"This is not Pedro's Greeter, right?");
    }
}
